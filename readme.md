# Praposes Data
Salah satu tahapan dasar dalam Datamining

# Cara Menggunakan
* clone/pull repository ini
* jalankan virtual environtment
* install package yang dibutuhkan `pip install -r requirements.txt`
* jalankan `jupyter notebook`
* buka notebook

# Data
* [Data Rumah Sakit Umum Per Kelas](https://data.go.id/dataset/data-rumah-sakit-umum-per-kelas) Milik Kementerian Kesehatan Indonesia
* [Data Tenaga Medis](https://data.go.id/dataset/data-tenaga-medis) milik Kementerian Kesehatan Indonesia
* [Data Puskesman 2008 - 2012](https://data.go.id/dataset/puskesmas-perawatan-dan-non-perawatan) milik Kementerian Kesehatan Indonesia
* [Data Puskesman 2013 - 2017](http://www.pusdatin.kemkes.go.id/resources/download/pusdatin/profil-kesehatan-indonesia/Data-dan-Informasi_Profil-Kesehatan-Indonesia-2017.pdf) milik Kementerian Kesehatan Indonesia
* [Indeks Pembangunan Manusia](http://ipm.bps.go.id/data/nasional) Milik Badan Pusat Statistik 

    Lisensi data adalah [Creative Common](http://opendefinition.org/licenses/cc-by/)

